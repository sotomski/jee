FROM maven:3 as builder

# copy only pom's to cache dependencies
WORKDIR /build
COPY ./pom.xml ./
COPY ./ear/pom.xml ./ear/
COPY ./ejbs/pom.xml ./ejbs/
COPY ./primary-source/pom.xml ./primary-source/
COPY ./projects/pom.xml ./projects/
COPY ./projects/logging/pom.xml ./projects/logging/
COPY ./servlets/pom.xml ./servlets/
COPY ./servlets/servlet/pom.xml ./servlets/servlet/
RUN mvn verify clean --fail-never
RUN mvn package --fail-never

# copy rest of the code
COPY . .
RUN mvn package

FROM jboss/wildfly:13.0.0.Final
ENV MYSQL_CONNECTOR_VERSION=8.0.11
ENV ACTIVEMQ_VERSION=5.9.1

# copy configuration
COPY --chown=jboss docker/jboss_home $JBOSS_HOME

# install mysql connector
#ADD --chown=jboss https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-$MYSQL_CONNECTOR_VERSION.zip /tmp/
#RUN cd /tmp && \
#    unzip mysql-connector-java-$MYSQL_CONNECTOR_VERSION.zip && \
#    cp mysql-connector-java-$MYSQL_CONNECTOR_VERSION/mysql-connector-java-$MYSQL_CONNECTOR_VERSION.jar $JBOSS_HOME/modules/com/mysql/main/

# install activemq-ra
#ADD --chown=jboss http://repo1.maven.org/maven2/org/apache/activemq/activemq-rar/$ACTIVEMQ_VERSION/activemq-rar-$ACTIVEMQ_VERSION.rar $JBOSS_HOME/standalone/deployments/

# copy ear
COPY --chown=jboss --from=builder /build/ear/target/*.ear $JBOSS_HOME/standalone/deployments/

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-c", "standalone-full.xml"]
