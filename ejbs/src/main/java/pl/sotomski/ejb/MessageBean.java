package pl.sotomski.ejb;

import javax.ejb.*;
import javax.jms.*;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "activemq/queue/TestQueue") })
public class MessageBean implements MessageListener, MessageDrivenBean {

    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                System.out.println("Got Message "
                        + ((TextMessage) message).getText());
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void setMessageDrivenContext(MessageDrivenContext messageDrivenContext) throws EJBException {

    }

    public void ejbRemove() throws EJBException {

    }
}
